import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { AboutModule } from './Layouts/about/Parent_files/about.module';

const routes: Routes = [
  {
    path: '', loadChildren: () => import("./Layouts/home/Parent_files/home.module")
      .then(mod => mod.HomeModule)
  },

  {
    path: 'about', loadChildren: () => import("./Layouts/about/Parent_files/about.module")
      .then(mod => mod.AboutModule)
  },

  {
    path: 'ourservices', loadChildren: () => import("./Layouts/ourservices/Parent_files/ourservices.module")
      .then(mod => mod.OurservicesModule)
  },
  {
    path: 'contact', loadChildren: () => import("./Layouts/contact/Parent_files/contact.module")
      .then(mod => mod.ContactModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
