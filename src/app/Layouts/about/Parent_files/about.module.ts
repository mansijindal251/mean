import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutRoutingModule } from './about-routing.module';
import { AboutCompComponent } from '../about-comp/about-comp.component';



@NgModule({
  declarations: [
    AboutCompComponent,
  ],
  imports: [
    CommonModule,
    AboutRoutingModule,
  ]
})
export class AboutModule { }
