import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OurservicesRoutingModule } from './ourservices-routing.module';
import { OurservicesCompComponent } from '../ourservices-comp/ourservices-comp.component';


@NgModule({
  declarations: [
    OurservicesCompComponent
  ],
  imports: [
    CommonModule,
    OurservicesRoutingModule
  ]
})
export class OurservicesModule { }
