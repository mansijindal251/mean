import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OurservicesCompComponent } from '../ourservices-comp/ourservices-comp.component';

const routes: Routes = [
  {path:'', component: OurservicesCompComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OurservicesRoutingModule { }
