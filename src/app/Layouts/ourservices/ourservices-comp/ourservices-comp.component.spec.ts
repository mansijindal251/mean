import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurservicesCompComponent } from './ourservices-comp.component';

describe('OurservicesCompComponent', () => {
  let component: OurservicesCompComponent;
  let fixture: ComponentFixture<OurservicesCompComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurservicesCompComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OurservicesCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
