import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatIconModule} from '@angular/material/icon';
import { SwiperModule } from 'swiper/angular';
import { MatCardModule } from '@angular/material/card';
import { HomeRoutingModule } from './home-routing.module';
import { HomeCompComponent } from '../home-comp/home-comp.component';
import {MatGridListModule} from '@angular/material/grid-list';

@NgModule({
  declarations: [
    HomeCompComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatIconModule,
    SwiperModule,
    MatCardModule,
    MatGridListModule
  ]
})
export class HomeModule { }
