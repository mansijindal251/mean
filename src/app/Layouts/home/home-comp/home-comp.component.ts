import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Platform } from '@angular/cdk/platform';

@Component({
  selector: 'app-home-comp',
  templateUrl: './home-comp.component.html',
  styleUrls: ['./home-comp.component.css']
})
export class HomeCompComponent {
  breakpoint: number | undefined;
  breakpoint2: number | undefined;
  breakpoint3: number | undefined;

  constructor(
    public platform: Platform
  ) { }
  ngOnInit() {
    this.breakpoint = (window.innerWidth <= 400) ? 1 : 2;
    this.breakpoint2 = (window.innerWidth <= 400) ? 1 : 3;
    this.breakpoint3 = (window.innerWidth <= 400) ? 1 : 4;

    console.log(this.breakpoint)
    console.log(this.breakpoint2)
  }
  
  onResize(event:any) {
    this.breakpoint = (event.target.innerWidth <= 400) ? 1 : 2;
    this.breakpoint2 = (event.target.innerWidth <= 400) ? 1 : 3;
    this.breakpoint3= (event.target.innerWidth <=400)? 1:4
  }

}
