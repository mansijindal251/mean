import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import { MatGridListModule } from '@angular/material/grid-list';
import { SharedRoutingModule } from './shared-routing.module';
import {FooterComponent} from "../footer/footer.component";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


@NgModule({
    declarations: [NavbarComponent, FooterComponent],
    exports: [NavbarComponent, FooterComponent],
    imports: [
        CommonModule,
        SharedRoutingModule,
        MatButtonModule,
        MatToolbarModule,
        RouterModule,
        MatIconModule,
        MatListModule,
        MatSidenavModule,
        MatGridListModule,
        FontAwesomeModule
    ]
})
export class SharedModule { }
