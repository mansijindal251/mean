import { Component } from '@angular/core';
import { faLinkedin, faInstagram, faFacebook, faGitlab } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
  myLinkedin =faLinkedin;
  myInstagram= faInstagram;
  myFacebook= faFacebook;
  myGitlab= faGitlab;
  
}
