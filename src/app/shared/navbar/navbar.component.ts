import { Component, HostBinding, HostListener } from '@angular/core';
import { Platform } from '@angular/cdk/platform';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  show:boolean=false;
  constructor(
    public platform: Platform
  ) { }
  toggle_me(){
    this.show= !this.show;
  }
}
